
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=yes">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
 
	<div class="container-fluid" style="margin-top: 100px">
 
		@yield('content')
		@yield('categories')
        @yield('products')
        @yield('create_category')
        @yield('create_product')
        @yield('edit_category')
        @yield('edit_product')
        @yield('list_categories')
        @yield('view_products')
        @yield('create_shopping_cart')
        @yield('shopping_carts')
        @yield('order_checkouts')
        @yield('empty_page')
        @yield('list_orders')
        @yield('order_detail')
	</div>
	<style type="text/css">
	.table {
		border-top: 2px solid #ccc;
 
	}
</style>
</body>
</html>
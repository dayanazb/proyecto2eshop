<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Eshop') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">
    <!-- Icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/home') }}">
                    {{ config('app.name', 'Eshop') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>
               
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     @if(Auth::user())
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                      <li class="nav-item" id="list_categories_li">
                        <a class="nav-link" href="/list_categories">Catálogo</a>
                      </li>
                      <li class="nav-item" id="shopping_cart_li">
                        <a class="nav-link" href="/shopping_cart">Carrito</a>
                      </li>
                      <li class="nav-item" id="shopping_cart_li">
                        <a class="nav-link" href="/list_orders">Ordenes</a>
                      </li>
                      @if(Auth::user()->admin)
                      <li class="nav-item dropdown" id="category_li">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" href="#">Configuración</a>
                        <div class="dropdown-menu">
                          <a class="dropdown-item" href="/category">Categoría</a>
                          <a class="dropdown-item" href="/product">Producto</a>
                        </div>
                      </li>
                      @else 
                        <a class="nav-link"  role="button" href="/empty_page">Configuración</a>
                      @endif
                    </ul>
                     @endif

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Inicio de sesión') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Registro') }}</a>
                            </li>
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
               
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
            @yield('categories')
            @yield('products')
            @yield('create_category')
            @yield('create_product')
            @yield('edit_category')
            @yield('edit_product')
            @yield('list_categories')
            @yield('view_products')
            @yield('create_shopping_cart')
            @yield('shopping_carts')
            @yield('order_checkouts')
            @yield('empty_page')
            @yield('list_orders')
            @yield('order_detail')
        </main>
    </div>
</body>
</html>

@extends('layouts.app')

@section('view_products')
  <div class="container">
      <div class="justify-content-center">
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de productos

                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">sku</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Stock</th>
                      <th scope="col">imagen</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $key)
                      <tr>
                        <th scope="row">{{ $key->sku }}</th>
                        <td>{{ $key->name }}</td>
                        <td>{{ $key->description }}</td>
                        <td>{{ $key->stock }}</td>
                        <td><img style="width: 150px; height: 100px;" src="/img/{{ $key->image }}" alt="image"></td>
                        <td>
                          <a href="/cart/{{ $key->id }}" id="{{ $key->id }}">Agregar al carrito</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection
@extends('layouts.app')

@section('shopping_carts')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('order') }}"><button type="button" class="btn btn-secondary btn-lg">Checkout</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Carrito de compra

                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Precio</th>
                      <th scope="col">Precio Total</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($shopping_carts as $key)
                      <tr>
                        <th scope="row">{{ $key->name}}</th>
                        <td>{{ $key->description }}</td>
                        <td>{{ $key->quantity_product}}</td>
                        <td>${{ $key->price_product }}</td>
                        <td>${{ $key->price_total}}</td>
                        <td>
                          <a href="#" id="{{ $key->id }}" class="a_delete">Eliminar</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
  <script src="{{ asset('js/shopping_cart.js') }}" defer></script>
@endsection
@extends('layouts.app')

@section('create_category')
  <div class="container">
      <div class="row justify-content-center">

          <div class="col-sm-8">
              <div class="card">
                  <div class="card-header">Formulario de Categoría</div>

                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif

                      {!! Form::open(['url' => '/category', 'method' => 'POST']) !!}
                      <div class="form-group">
                        {!! Form::label('description', 'Descripcion') !!}
                        {!! Form::text('description', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                      </div>

                      <div class="form-group">
                        <label for="id_father">Categoría Padre:</label>
                          <select class="form-control" name="id_father">
                              @foreach($categories as $category)
                              <option value="{{$category->id}}">{{$category->description}}</option>
                              @endforeach
                          </select>
                      </div>
                      {!! Form::submit('Registrar', ['class' => 'btn btn-info']) !!}

                      {!! Form::close() !!}

                  </div>
              </div>
          </div>

      </div>
  </div>
@endsection

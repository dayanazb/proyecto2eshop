@extends('layouts.app')

@section('list_orders')
  <div class="container">
      <div class="justify-content-center">
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de ordenes
                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Fecha</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Total</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($orders as $key)
                      <tr>
                        <th scope="row">{{ $key->order_date }}</th>
                        <th scope="row">{{ $key->name }}</th>
                        <th scope="row">${{ $key->total }}</th>
                        <td>
                          <a href="/order_detail/{{ $key->id }}" id="{{ $key->id }}">Ver</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection

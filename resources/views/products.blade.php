@extends('layouts.app')

@section('products')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('product/create') }}"><button type="button" class="btn btn-secondary btn-lg">Agregar Producto</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de productos

                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">sku</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Descripcion</th>
                      <th scope="col">imagen</th>
                      <th scope="col">Categoría</th>
                      <th scope="col">Stock</th>
                      <th scope="col">Precio</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $key)
                      <tr>
                        <th scope="row">{{ $key->sku }}</th>
                        <td>{{ $key->name }}</td>
                        <td>{{ $key->description }}</td>
                        <td><img style="width: 150px; height: 100px;" src="/img/{{ $key->image }}" alt="image"></td>
                        @foreach($categories as $cat)
                          @if($key->id_category === $cat->id)
                            <td>{{ $cat->description }}</td>
                            @break
                          @endif
                        @endforeach
                        <td>{{ $key->stock }}</td>
                        <td>${{ $key->price }}</td>
                        <td>
                          <a href="#" id="{{ $key->id }}" class="a_delete">Eliminar</a>
                          <a href="/product/{{ $key->id }}/edit" id="{{ $key->id }}" class="a_edit">Edit</a>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $products->links() }}
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
  <script src="{{ asset('js/product.js') }}" defer></script>
@endsection

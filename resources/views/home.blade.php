@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Estadísticas</div>

                <div class="card-body">
                    @if(Auth::user()->admin)
                    <table class="table table-striped">
                        <tr>
                            <th>Cantidad de clientes registrados</th>
                        </tr>
                        <tr>
                            <th >{{ $users }}</th>
                        </tr>  
                        <tr>
                            <th>Cantidad de productos vendidos</th>
                        </tr>
                        <tr>
                            <th >{{ $quantity_products }}</th>
                        </tr> 
                        <tr>
                            <th>Monto total de ventas</th>
                        </tr>
                        <tr>
                            <th >{{ $order }}</th>
                        </tr> 
                    </table>
                    @else 
                        <table class="table table-striped">
                        <tr>
                            <th>Total de productos adquiridos por el cliente</th>
                        </tr>
                        <tr>
                            <th >{{ $user_produts }}</th>
                        </tr>  
                        <tr>
                            <th>Monto total de compras realizadas por el cliente</th>
                        </tr>
                        <tr>
                            <th >{{ $total_orders_user }}</th>
                        </tr> 
                    </table>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

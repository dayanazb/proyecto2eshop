@extends('layouts.app')

@section('order_detail')
  <div class="container">
      <div class="justify-content-center">
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Detalle de la orden
                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Fecha</th>
                      <th scope="col">Total</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($orders as $key)
                      <tr>
                        <th scope="row">{{ $key->order_date }}</th>
                        <th scope="row">${{ $key->total }}</th>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="card-header">
                  <div class="row">
                    <div class="col-md-8">
                      Productos
                    </div>
                  </div>
                </div>
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Descripción</th>
                      <th scope="col">Precio</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($orders_cart as $key)
                      <tr>
                        <td >{{ $key->quantity_product }}</td>
                        <td >{{ $key->description_product }}</td>
                        <td >${{ $key->price_product }}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection
@extends('layouts.app')

@section('create_product')
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-sm-8">
				<div class="card">
					<div class="card-header">Formulario de Producto
					</div>
					<div class="card-body">
						@if (session('status'))
						<div class="alert alert-success" role="alert">
							{{ session('status') }}
						</div>
						@endif

						{!! Form::open(['url' => '/product', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

					    <div class="form-group">
	                        {!! Form::label('sku', 'Sku') !!}
	                        {!! Form::text('sku', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group">
	                        {!! Form::label('name', 'Nombre') !!}
	                        {!! Form::text('name', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group">
	                        {!! Form::label('description', 'Descripción') !!}
	                        {!! Form::text('description', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                        </div>
                        <div class="form-group">
			              <label for="image">Imagen</label>
			              <div class="col-md-6">
			                <input type="file" class="form-control" name="image" >
			              </div>
			            </div>
                        <div class="form-group">
	                        <label for="id_category">Categoría:</label>
	                          <select class="form-control" name="id_category">
	                              @foreach($categories as $category)
	                              <option value="{{$category->id}}">{{$category->description}}</option>
	                              @endforeach
	                          </select>
                       </div>
                       <div class="form-group">
	                        {!! Form::label('stock', 'Stock') !!}
	                        {!! Form::number('stock', '', ['placeholder' => '', 'class' => 'form-control','min' => '0',
	                         'required']) !!}
                        </div>
                        <div class="form-group">
	                        {!! Form::label('price', 'Precio') !!}
	                        {!! Form::number('price', '', ['placeholder' => '', 'class' => 'form-control','min' => '0', 'required']) !!}
                        </div>
                        {!! Form::submit('Registrar', ['class' => 'btn btn-info']) !!}

                      {!! Form::close() !!}

					</div>

				</div>
			</div>

		</div>
		
	</div>
	@endsection
@extends('layouts.app')

@section('order_checkouts')
  <div class="container">
      <div class="justify-content-center">
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                   Realizar Pago

                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Nombre</th>
                      <th scope="col">Cantidad</th>
                      <th scope="col">Precio</th>
                      <th scope="col">Precio Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($order_checkouts as $key)
                      <tr>
                        <th scope="row">{{ $key->name}}</th>
                        <td>{{ $key->quantity_product}}</td>
                        <td>${{ $key->price_product }}</td>
                        <td>${{ $key->price_total }} </td>
                      </tr>
                    @endforeach
                    <br>
                    <br>
                  </tbody>
                </table>
              </div>
            </div>
            <div>
              {!! Form::open(['url' => '/order', 'method' => 'POST']) !!}
              <table class="p-3 mb-2 bg-dark text-white" >
                <tr>
                  <td><label>Nombre de la orden:</label></td>
                  <td><input type="text" name="name_order" required autofocus></td>
                </tr>
                <br>
                <tr>
                  <td><label>Nombre:</label></td>
                  <td><input type="text" name="nombre" required autofocus></td>
                </tr>
                <tr>
                  <td><label>Numero de Tarjeta:</label></td>
                  <td><input type="text" name="num_tarjeta" required autofocus></td>
                </tr>
                <tr>
                  <td><label>Codigo de seguridad:</label></td>
                  <td><input type="text" name="codigo" required autofocus></td>
                </tr>
                
              </table>
              {!! Form::submit('Checkout', ['class' => 'btn btn-secondary btn-lg']) !!}

                      {!! Form::close() !!}
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection
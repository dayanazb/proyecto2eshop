@extends('layouts.app')

@section('create_shopping_cart')
  <div class="container">
      <div class="row justify-content-center">

          <div class="col-sm-8">
              <div class="card">
                  <div class="card-header">Agregar producto al carrito</div>

                  <div class="card-body">
                      @if (session('status'))
                          <div class="alert alert-success" role="alert">
                              {{ session('status') }}
                          </div>
                      @endif

                      {!! Form::open(['url' => '/shopping_cart', 'method' => 'POST']) !!}
                      <div class="form-group col-md-8">
                            <label for="id_user"><strong>User:</strong></label>  <label>{{Auth::user()->name}}</label>
                            <input type="text"   name="id_user" hidden readonly="true" value="{{ Auth::user()->id}}" >
                          </div>
                          <div class="form-group col-md-8 ">
                            <label for="id_product"><strong>Producto:</strong></label> <label>{{$product->name}}</label>
                            <input type="text"  name="id_product" hidden value="{{$product->id}}">
                          </div>
                          <div class="form-group col-md-8 ">
                            <label for="description_product"><strong>Descripción:</strong></label>  <label>{{$product->description}}</label>
                            <input type="text"  name="description_product" hidden value="{{$product->description}}">
                          </div>
                          <div class="form-group col-md-8 ">
                            <label for="price_product"><strong>Precio:</strong></label>  <label>{{$product->price}}</label>
                            <input type="text"  name="price_product" hidden value="{{$product->price}}">
                          </div>
                          <div class="form-group col-md-8">
                            <label for="stock"><strong>Disponible:</strong></label>  <label>{{$product->stock}}</label>
                            <input type="number" name="stock" hidden value="{{$product->stock}}" >
                          </div>
                      <div class="form-group col-md-8 ">
                       <strong>{!! Form::label('quantity_product', 'Cantidad:') !!}</strong> 
                        {!! Form::number('quantity_product', '', ['placeholder' => '', 'class' => 'form-control', 'min' => '0', 'max' => $product->stock, 'required']) !!}
                      </div>
                      {!! Form::submit('Agregar', ['class' => 'btn btn-info']) !!}

                      {!! Form::close() !!}

                  </div>
              </div>
          </div>

      </div>
  </div>
@endsection

@extends('layouts.app')

@section('categories')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('category/create') }}"><button type="button" class="btn btn-secondary btn-lg">Agregar Categoría</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de categorias
                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Descripcion</th>
                      <th scope="col">Categoría Padre</th>
                      <th scope="col">Acciones</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($categories as $key)
                        @if($key->description != "Ninguna")
                          <tr>
                            <th scope="row">{{ $key->description }}</th>
                            @foreach($categories as $cat)
                              @if($key->id_father === $cat->id)
                                <td>{{ $cat->description }}</td>
                                @break
                              @endif
                            @endforeach
                            <td>
                              <a href="#" id="{{ $key->id }}" class="a_delete">Eliminar</a>
                              <a href="/category/{{ $key->id }}/edit" id="{{ $key->id }}" class="a_edit">Edit</a>
                            </td>
                          </tr>
                        @endif
                    @endforeach
                  </tbody>
                </table>
                {{ $categories->links() }}
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
  <script src="{{ asset('js/category.js') }}" defer></script>
@endsection

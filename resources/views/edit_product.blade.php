@extends('layouts.app')

@section('products')
    <div class="container">
        {!! Form::open(['url' => '/product/'.$product->id, 'method' => 'put', 'enctype' => 'multipart/form-data']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Editar Producto</h2>
              			<div class="card-header">

                  					<div class="row">
							          <div class="col-md-"></div>
							         	<div class="form-group col-md-8">
							            	<label for="sku">Sku:</label>
							            	<input type="text"   name="sku" value="{{$product->sku}}">
							          	</div>
							          	<div class="form-group col-md-8 ">
							            	<label for="name">Nombre:</label>
							            	<input type="text"  name="name" value="{{$product->name}}">
							          	</div>
							          	<div class="form-group col-md-8">
							            	<label for="description">Descripción:</label>
							            	<input type="text"  name="description" value="{{$product->description}}">
							          	</div>
							          	<div class="form-group col-md-8">
					             			<label for="image">Imagen:</label>
					              			<div class="col-md-6">
					                			<input type="file"  name="image" value="{{$product->image}}" >
					              			</div>
					           			</div>
								        <div class="form-group col-md-8 ">
					                        <label for="id_category">Categoría:</label>
					                          <select  name="id_category">
					                              @foreach($categories as $category)
					                              <option value="{{$category->id}}">{{$category->description}}</option>
					                              @endforeach
					                          </select>
					                     </div>	
					                     <div class="form-group col-md-8 ">
							            	<label for="stock">Stock:</label>
							            	<input type="number"  name="stock" value="{{$product->stock}}">
							          	</div>
							          	<div class="form-group col-md-8">
							            	<label for="price">Precio:</label>
							            	<input type="text"  name="price" value="{{$product->price}}">
							          	</div>

							        </div>

							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Actualizar</button>
							          </div>
							        </div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection
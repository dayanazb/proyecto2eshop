$(document).ready(function(){

});

var Shopping_cart = {
	delete: function(shopping_cart_id){
    return $.getJSON('/shopping_cart/'+shopping_cart_id+'/delete');
  },
}

$('.a_delete').on("click",function(){
	
    var confirmLeave = confirm('¿Seguro que desea eliminar este producto?');
	if (confirmLeave==true)
	{
  		var shopping_cart_id = $(this).attr("id");
  		Shopping_cart.delete(shopping_cart_id).done(function(json){
  			if (json.code == 200) {
  				location.reload();
  			}
  		});
	}
});

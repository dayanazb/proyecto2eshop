$(document).ready(function(){

});

var Category = {
	delete: function(category_id){
    return $.getJSON('/category/'+category_id+'/delete');
  },

}

$('.a_delete').on("click",function(){
	
    var confirmLeave = confirm('¿Seguro que desea eliminar categoría?');
	if (confirmLeave==true)
	{
  		var category_id = $(this).attr("id");
  		Category.delete(category_id).done(function(json){
  			if (json.code == 200) {
          console.log(json);
          alert(json.message);
  				location.reload();
  			}else{
          alert('Categoría tiene productos enlazados.');
        }
  		});
	}
});

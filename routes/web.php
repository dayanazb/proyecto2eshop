<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::resource('category','CategoryController');
Route::get('list_categories','CategoryController@index_categories');
Route::get('empty_page','CategoryController@empty_page');
Route::get('/view_products/{id}','CategoryController@getProducts');
Route::get('/category/{id}/delete', 'CategoryController@delete');
Route::resource('product','ProductController');
Route::get('/product/{id}/delete', 'ProductController@delete');
Route::resource('shopping_cart','ShoppingCartController');
Route::get('/cart/{id}','ShoppingCartController@create');
Route::get('/shopping_cart/{id}/delete', 'ShoppingCartController@delete');
Route::resource('order','OrderController');
Route::get('list_orders','OrderController@index_orders');
Route::get('/order_detail/{id}','OrderController@order_detail');

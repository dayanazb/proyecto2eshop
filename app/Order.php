<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['name', 'id_user', 'total', 'order_date', 'quantity_product'];
}


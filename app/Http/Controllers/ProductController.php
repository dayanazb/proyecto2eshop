<?php

namespace App\Http\Controllers;
use App\Product;
use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use DB; 
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
        $products = DB::table('products')->orderBy('id', 'desc')->paginate(10);
        $categories = DB::table('categories')->orderBy('id', 'desc')->paginate(10);
        return view('products')->with(['products' => $products, 'categories' => $categories]);
    }
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = DB::table('products')->orderBy('id', 'desc')->paginate(10);
        $categories = DB::table('categories')->orderBy('id', 'desc')->paginate(10);
        return view('create_product')->with(['products' => $products, 'categories' => $categories]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // ruta de las imagenes guardadas
        $ruta = public_path().'/img/';
 
        // recogida del form
        $imagenOriginal = $request->file('image');
 
        // crear instancia de imagen
        $imagen = Image::make($imagenOriginal);
 
        // generar un nombre aleatorio para la imagen
        $temp_name = $this->random_string() . '.' . $imagenOriginal->getClientOriginalExtension();
 
        $imagen->resize(300,300);
 
        // guardar imagen
        // save( [ruta], [calidad])
        $imagen->save($ruta . $temp_name, 100);

        $c = new Product;
        $c->sku = $request->sku;
        $c->name = $request->name;
        $c->description = $request->description;
        $c->image = $temp_name;
        $c->id_category = $request->id_category;
        $c->stock = $request->stock;
        $c->price = $request->price;

        if ($c->save()) {
          return redirect()->action('ProductController@index');
        }
    }
 
    protected function random_string()
    {
        $key = '';
        $keys = array_merge( range('a','z'), range(0,9) );
     
        for($i=0; $i<10; $i++)
        {
            $key .= $keys[array_rand($keys)];
        }
     
        return $key;
    }

    public function edit($id){
        $product = Product::find($id);
        $products = DB::table('products')->orderBy('id', 'desc')->paginate(10);
        $categories = DB::table('categories')->orderBy('id', 'desc')->paginate(10);
        return view('edit_product')->with(['product' => $product, 'products' => $products, 'categories' => $categories]);
    }

    public function update(Request $request, $id){
          // ruta de las imagenes guardadas
        $ruta = public_path().'/img/';
        
        // recogida del form
        $imagenOriginal = $request->file('image');
 
        // crear instancia de imagen
        $imagen = Image::make($imagenOriginal);
 
        // generar un nombre aleatorio para la imagen
        $temp_name = $this->random_string() . '.' . $imagenOriginal->getClientOriginalExtension();
 
        $imagen->resize(300,300);
 
        // guardar imagen
        // save( [ruta], [calidad])
        $imagen->save($ruta . $temp_name, 100);

        DB::table('products')->where('id', $id)->update(['sku' => $request->sku, 'name' => $request->name, 'description' => $request->description, 'image' => $temp_name, 'id_category' => $request->id_category, 'stock' => $request->stock, 'price' => $request->price]);
        $products = DB::table('products')->orderBy('id', 'desc')->paginate(10);
        return view('products')->with('products', $products);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('products')->where('id', $id)->delete();
        return response()->json(['code' => 200]);
    }
}



<?php

namespace App\Http\Controllers;

use App\Product;
use App\ShoppingCart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {    
         $user_id = auth()->id(); 
        $shopping_carts = DB::table('shopping_carts')
            ->join('products', 'shopping_carts.id_product', '=', 'products.id')
            ->join('users', 'shopping_carts.id_user', '=', 'users.id')
            ->select('users.*', 'products.*','shopping_carts.*')
            ->where('shopping_carts.id_user', '=',  $user_id)
            ->get();
        return view('shopping_carts')->with('shopping_carts', $shopping_carts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {   
        $product = Product::find($id);
        return view('create_shopping_cart')->with(['product' => $product]);
    }

     /**
     * Store a newly created resource in storage.
     *
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Disminuir el stock
        $stock = $request->stock - $request->quantity_product;
        DB::table('products')->where('id', $request->id_product)->update(['stock' => $stock]);
        
        $c = new ShoppingCart;
        $c->id_user = $request->id_user;
        $c->quantity_product = $request->quantity_product;
        $c->id_product = $request->id_product;
        $c->price_product = $request->price_product;
        $c->description_product = $request->description_product;
        $c->price_total = $request->quantity_product * $request->price_product;
        if ($c->save()) {
          return redirect()->action('ShoppingCartController@index');
        }
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $stock = $request->quantity_product + $request->stock;
        DB::table('products')->where('id', $request->id_product)->update(['stock' => $stock]);
          return redirect()->action('ShoppingCartController
            ');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $product = DB::table('shopping_carts')->where('id', $id)->first(); 
        $product_stock = Product::find($product->id_product);
        $stock = $product_stock->stock + $product->quantity_product;
        //Aumentar el stock cuando se elimina el producto de carrito
        DB::table('products')->where('id', $product->id_product)->update(['stock' => $stock]);
        DB::table('shopping_carts')->where('id', $id)->delete();
        return response()->json(['code' => 200]);
    }
}


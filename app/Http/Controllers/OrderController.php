<?php

namespace App\Http\Controllers;

use App\Product;
use App\ShoppingCart;
use App\Order;
use App\OrderCart;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class orderController extends Controller
{
   /**
     *  Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->id(); 
        $order_checkouts = DB::table('shopping_carts')
            ->join('products', 'shopping_carts.id_product', '=', 'products.id')
            ->join('users', 'shopping_carts.id_user', '=', 'users.id')
            ->select('users.*', 'products.*','shopping_carts.*')
            ->where('shopping_carts.id_user', '=',  $user_id)
            ->get();
        return view('order_checkouts')->with('order_checkouts', $order_checkouts);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_orders()
    {   
        $user_id = auth()->id(); 
        $orders = DB::table('orders')->where('id_user', $user_id)->get();
        return view('list_orders')->with('orders', $orders);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function order_detail($id)
    {   
        $user_id = auth()->id(); 
        $orders = DB::table('orders')->where('id', $id)->get();
        $orders_cart = DB::table('order_carts')->where('id_order', $id)->get();
       // dd($orders_cart);
        return view('order_detail')->with(['orders' => $orders, 'orders_cart' => $orders_cart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $c = new Order;
        $user_id = auth()->id(); 
        $items = DB::table('shopping_carts')->where('id_user', $user_id)->get();
        $c->name = $request->name_order;
        $c->id_user = $user_id;
        $c->order_date = 'now()';
        foreach ($items as $key ) {
          $c->total += $key->price_total;
          $c->quantity_product += $key->quantity_product;
        }
        $c->id = DB::table('orders')->insertGetId(
          ['name' => $c->name, 'id_user' => $c->id_user, 'order_date' => $c->order_date, 'total' => $c->total, 'quantity_product' => $c->quantity_product]
        );

        foreach ($items as $key ) {
        $a = new OrderCart;
          //Agregar a orden carrito
          $a->id_user = $key->id_user;
          $a->quantity_product = $key->quantity_product;
          $a->price_product = $key->price_product;
          $a->description_product = $key->description_product;
          $a->total = $key->price_total;
          $a->id_order = $c->id;
          $a->save();
        }

          DB::table('shopping_carts')->where('id_user',  $user_id)->delete();
          return redirect()->action('OrderController@index_orders');
    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        DB::table('shopping_carts')->where('id', $id)->delete();
        return response()->json(['code' => 200]);
    }

   
}

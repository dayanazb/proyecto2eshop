<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\User;
use App\Http\Controllers\Controller;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     * Cantidad de productos vendidos.--admin
     * Monto total de ventas---admin
     * Cantidad de usuarios registrados--admin
     * Total de productos adquiridos por el cliente
     * Monto total de compras realizadas por el cliente
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = auth()->id(); 
        //Monto total de compras realizadas por el cliente
        $total_orders_user= DB::table('orders')
        ->where('id_user', $user_id)
        ->sum('total');
        //Total de productos adquiridos por el cliente
        $user_produts = DB::table('orders')
        ->where('id_user', $user_id)
        ->sum('quantity_product');
        //Monto total de ventas
        $order = DB::table('orders')
        ->sum('total');
        //Cantidad de productos vendidos
        $quantity_products = DB::table('orders')
        ->sum('quantity_product');
        //Cantidad de Clientes Registrados
         $users = DB::table('users')
        ->count('id');
        //dd($users);
        return view('home')->with(['total_orders_user' => $total_orders_user, 'user_produts' => $user_produts, 'order' => $order, 'quantity_products' => $quantity_products, 'users' => $users]);
    }
}

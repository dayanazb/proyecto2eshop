<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $fillable = ['id_user', 'quantity_product', 'id_product', 'price_product', 'description_product'];
}

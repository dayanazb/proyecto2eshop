<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCart extends Model
{
    protected $fillable = ['id_user', 'quantity_product', 'price_product', 'description_product', 'id_order', 'total'];
}
